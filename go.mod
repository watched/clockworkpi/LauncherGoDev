module github.com/clockworkpi/LauncherGoDev

go 1.17

require (
	github.com/cuu/gogame v0.0.0-20211011085728-70b3e040be73
	github.com/cuu/grab v2.0.0+incompatible
	github.com/cuu/wpa-connect v1.5.5
	github.com/fatih/structs v1.1.0
	github.com/go-ini/ini v1.63.2
	github.com/godbus/dbus v4.1.0+incompatible
	github.com/godbus/dbus/v5 v5.0.6
	github.com/itchyny/volume-go v0.2.1
	github.com/mattn/go-sqlite3 v1.14.9
	github.com/mitchellh/go-homedir v1.1.0
	github.com/muka/go-bluetooth v0.0.0-20211023182635-c49e7c952f19
	github.com/sirupsen/logrus v1.8.1
	github.com/veandco/go-sdl2 v0.4.10
	github.com/vjeantet/jodaTime v1.0.0
	github.com/yookoala/realpath v1.0.0
	github.com/zyxar/argo v0.0.0-20210923033329-21abde88a063
)

require (
	github.com/go-ole/go-ole v1.2.6 // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/moutend/go-wca v0.2.0 // indirect
	github.com/op/go-logging v0.0.0-20160315200505-970db520ece7 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	golang.org/x/sys v0.0.0-20211025201205-69cdffdb9359 // indirect
)
